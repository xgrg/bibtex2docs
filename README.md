# bibtex2docs: insert BibTeX references in Google Docs

[![pipeline status](https://gitlab.com/xgrg/bibtex2docs/badges/master/pipeline.svg)](https://gitlab.com/xgrg/bibtex2docs/commits/master)
[![coverage report](https://gitlab.com/xgrg/bibtex2docs/badges/master/coverage.svg)](https://gitlab.com/xgrg/bibtex2docs/commits/master)
[![downloads](https://img.shields.io/pypi/dm/bibtex2docs.svg)](https://pypi.org/project/bibtex2docs/)
[![python versions](https://img.shields.io/pypi/pyversions/bibtex2docs.svg)](https://pypi.org/project/bibtex2docs/)
[![pypi version](https://img.shields.io/pypi/v/bibtex2docs.svg)](https://pypi.org/project/bibtex2docs/)




**Date**: 2019, Aug 27 **Version**: 0.1

**Useful links**:
[Binary Installers](https://pypi.org/project/bibtex2docs)
[Source Repository](https://gitlab.com/xgrg/bibtex2docs)
[Issues & Ideas](https://gitlab.com/xgrg/bibtex2docs/issues)


## Overview

`bibtex2docs` finds references in a Google Docs document matching entries
from a BibTeX source file and replaces them either with the name of their first
author (with year of publication) or with numbers linked to a reference list.

## Installing

### Python version support

Officially Python 3.7.


### Installing from PyPI

`bibtex2docs` can be installed via pip from [PyPI] (https://pypi.org/project/bibtex2docs)


```bash
pip install bibtex2docs
```

### Getting started: enabling the Google API

`bibtex2docs` relies on calls to the Google Docs/Drive API to copy/
modify documents. Therefore it requires some credentials/permissions to run properly.

The first step is to enable the Google API and obtain credentials. This is done
following the instructions [there](https://developers.google.com/docs/api/quickstart/python). Follow the
link, enable the Google Docs API and download the client configuration
(`credentials.json`).

On first execution, access permissions will be asked to modify/create documents.
Access token will be saved on local disk as a `pickle` object (see [example](https://developers.google.com/docs/api/quickstart/python))

### Including references in the manuscript

`bibtex2docs` looks for reference ID starting with an `@`. Hence to include a
reference from a BibTeX file, insert the BibTeX ID of that reference in the manuscript preceded by an `@`.

Example:

> As suggested by @FirstAuthor2018, (...)

The ID will be replaced either by a reference number (according to the full
reference list) or by *First Author et al., 2019*. The full list of included references will be compiled and added to the document, only if the tag `{{bibliography}}` is found somewhere in the text.

## Dependencies

`bibtex2docs` requires the following dependencies (installed by `pip`):

- *python* v3.7+
- google-api-python-client>=1.7.11
- google-auth-httplib2>=0.0.3
- google-auth-oauthlib>=0.4.0
- nameparser>=1.0.4
- bibtexparser>=1.1.0


For development purposes:

- *python-nose* v1.2.1+ to run the unit tests
- *coverage* v3.6+


## Examples


**Using the `FirstAuthor et al., 2019` format**

Use the following command:

`bibtex2docs.py DOCUMENT_ID BIBTEX_FILE CREDENTIALS`

A copy of the document (with prefix `[BibTeX2Docs]` will be created and reference IDs will be replaced as in the following figure:

![example1](https://gitlab.com/xgrg/bibtex2docs/raw/master/doc/example1.png)


**Replacing with numbers**

Use the following command:

`bibtex2docs.py DOCUMENT_ID BIBTEX_FILE CREDENTIALS -N`

A copy of the document (with prefix `[BibTeX2Docs]` will be created and reference IDs will be replaced as in the following figure:

![example1](https://gitlab.com/xgrg/bibtex2docs/raw/master/doc/example2.png)

**Including full reference list**

If the document contains the tag `{{bibliography}}`, `bibtex2docs` will replace
it with a reference list including all the ones found in the document. The
format will depend on the selected reference mode (with numbers or not).

Alphabetically ordered (without numbers):
![example1](https://gitlab.com/xgrg/bibtex2docs/raw/master/doc/example3.png)

With reference numbers:
![example1](https://gitlab.com/xgrg/bibtex2docs/raw/master/doc/example4.png)

## Disclaimer

This software is provided as is without warranty, it may be subject to any changes
from the Google API or any of its dependencies. The software will never apply
in-place modifications to documents and will always work on copies. Still we
recommend to make backup copies of any document before using the software.
